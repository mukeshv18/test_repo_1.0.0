#! /usr/bin/env groovy

def branchName = "${env.BRANCH_NAME}"
def jobname = "${env.JOB_NAME}"
def buildno = "${env.BUILD_NUMBER}"
def deployment
def stagelog
def mavenlog
def testlog
def ArtifactUrl
def label = "${branchName}".replaceAll('-', '_').replaceAll('/', '_').replaceAll(' ', '_') + "_label"
def cloudId = 'ds-cicd'
def namespace = 'ds-cicd'
def workingdir = "/home/jenkins"

properties([parameters([choice(choices: ['Yes', 'No'], description: 'Select if want to run Veracode', name: 'Run_Veracode')])])

properties([disableConcurrentBuilds(),buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10'))])

milestone()
pipeline{
   agent any
   stages{
      stage("GIT Checkout"){
	     steps{
		   git credentialsId: 'Bitbucket', url: 'https://bitbucket.org/mukeshv18/test_repo/src/master/'
		   }
		}
		stage("Build"){
	     steps{
		   echo 'pulling the branch ${params.Run_Veracode}'
		   }
		}
	}
}